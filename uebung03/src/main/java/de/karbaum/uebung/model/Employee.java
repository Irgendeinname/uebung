package de.karbaum.uebung.model;

import de.karbaum.uebung.controller.DateController;

import java.util.Date;

/**
 * Created by dev on 29.06.16.
 */
public class Employee
{
    public enum GenderType
    {
        M, F;
    }

    private Integer emp_no;
    private Date birth_date;
    private String first_name;
    private String last_name;
    private GenderType  gender;
    private Date hire_date;

    public Employee()
    {
    }

    public Employee(Integer emp_no, Date birth_date, String first_name, String last_name, GenderType gender, Date hire_date)
    {
        this.emp_no = emp_no;
        this.birth_date = birth_date;
        this.first_name = first_name;
        this.last_name = last_name;
        this.gender = gender;
        this.hire_date = hire_date;
    }

    @Override
    public boolean equals(Object obj)
    {
        // wenn als Objekt null übergeben wird, dann ist der Vergleich "false"
        if (obj == null)
            return false;

        // wenn das Objekt mit sich selbst verglichen wird, dann ist der Vergleich "true"
        if (obj == this)
            return true;

        // wir vergleichen Elemente feldweise
        Employee tmp = (Employee) obj;

        // 1. Vergleich emp_no
        if (!this.emp_no.equals(tmp.emp_no))
            return false;

        // 2. Vergleich birth_date
        if (!this.birth_date.equals(tmp.birth_date))
            return false;

        // 3. Vergleich first_name
        if (!this.first_name.equals(tmp.first_name))
            return false;

        // 4. Vergleich last_name
        if (!this.last_name.equals(tmp.last_name))
            return false;

        // 5. Vergleich gender
        if (!this.gender.equals(tmp.gender))
            return false;

        // 6. Vergleich hire_date
        if (!this.hire_date.equals(tmp.hire_date))
            return false;

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = 5381;

        result = result * 37 + emp_no.hashCode();
        result = result * 37 + birth_date.hashCode();
        result = result * 37 + first_name.hashCode();
        result = result * 37 + last_name.hashCode();
        result = result * 37 + gender.hashCode();
        result = result * 37 + hire_date.hashCode();

        return result;
    }

    public String asCSV()
    {
        return String.format("%s, %s, %s, %s, %s, %s",
                this.emp_no,
                DateController.toString(this.birth_date),
                this.first_name,
                this.last_name,
                this,gender,
                DateController.toString(this.hire_date));
    }
}
