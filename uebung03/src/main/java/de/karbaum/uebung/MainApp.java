package de.karbaum.uebung;

import de.karbaum.uebung.controller.EmployeeController;

/**
 * Created by dev on 27.06.16.
 */
public class MainApp
{
    public static void main(String[] args)
    {
        EmployeeController employeeController = new EmployeeController();

        employeeController.retrieveEmployees();
    }
}