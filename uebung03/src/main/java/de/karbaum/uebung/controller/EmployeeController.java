package de.karbaum.uebung.controller;

import de.karbaum.uebung.model.Employee;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.*;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.Date;

/**
 * Created by dev on 29.06.16.
 */
public class EmployeeController
{
    private Connection conn;
    private Set<Employee> employeeSet;

    public EmployeeController()
    {
        // alle Mitarbeiter in einem Set<> verwalten
        employeeSet = new HashSet<>();

        Properties properties = new Properties();

        properties.put("user", "root");
        properties.put("password", "root");

        try
        {
            this.conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/employees", properties);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    public void retrieveEmployees()
    {
        if (conn == null)
            throw new RuntimeException("Keine Verbindung zur Datenbank");

        PreparedStatement pstm = null;

        try
        {
            String query = "SELECT * FROM employees LIMIT ?, ?";
            pstm = conn.prepareStatement(query);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        boolean completed = false;

        // 100 Datensätze aus der Datenbank holen, bis das Ende erreicht wurde (empty Set)
        for (int offset = 0; !completed; offset += 100)
        {
            try
            {
                pstm.setInt(1, offset);
                pstm.setInt(2, 100);

                ResultSet rs = pstm.executeQuery();

                // prüfen, ob fertig
                if (rs.next() == false)
                {
                    completed = true;
                    continue;
                }

                // falls noch nicht fertig
                rs.beforeFirst();

                System.out.println("\t<<< Chunk >>>");

                while (rs.next())
                {
                    // Vorbereiten der Daten für das Erzeugen der Employee-Objekte
                    Integer emp_no = rs.getInt(1);
                    Date birth_date = DateController.fromString(rs.getString(2));
                    String first_name = rs.getString(3);
                    String last_name = rs.getString(4);
                    Employee.GenderType gender = rs.getString(5).equals("M") ? Employee.GenderType.M : Employee.GenderType.F;
                    Date hire_date = DateController.fromString(rs.getString(6));

                    // neuer Mitarbeiter als Objekt
                    Employee emp = new Employee(emp_no, birth_date, first_name, last_name, gender, hire_date);

                    // Mitarbeiter wird in das Set<> eingetragen
                    employeeSet.add(emp);
                }

                writeToCSV();
                rs.close();
            }
            catch (SQLException e)
            {
                e.printStackTrace();
            }
        }
    }

    private void writeToCSV()
    {
        try(BufferedWriter writer = new BufferedWriter(new FileWriter("employees.csv", true)))
        {
            // für alle 100 Datensätze des Set<>
            for (Employee e: employeeSet)
            {
                // schreibt das Objekt als CSV-Zeile
                writer.write(e.asCSV());
                writer.newLine();
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        // Chunk wird aus dem Set<> gelöscht
        employeeSet.clear();
    }
}
