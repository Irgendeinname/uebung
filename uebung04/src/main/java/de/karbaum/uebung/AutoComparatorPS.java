package de.karbaum.uebung;

import java.util.Comparator;

/**
 * Created by dev on 01.07.16.
 */
public class AutoComparatorPS implements Comparator<Auto>
{
    @Override
    public int compare(Auto o1, Auto o2)
    {
        return o1.getMotorleistung() - o2.getMotorleistung();
    }
}