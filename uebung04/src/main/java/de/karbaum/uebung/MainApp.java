package de.karbaum.uebung;

import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeSet;

/**
 * Created by dev on 27.06.16.
 */
public class MainApp
{
    public static String line()
    {
        String line = "____________________________________";
        return line;
    }

    public static void main(String[] args)
    {
        // Ganze Zahlen
        Integer a = 100;
        Integer b = 400;
        Integer c = 800;
        Integer d = 1000;

        TreeSet<Integer> treeSet = new TreeSet<>();

        treeSet.add(c);
        treeSet.add(d);
        treeSet.add(a);
        treeSet.add(b);

        for (Integer i: treeSet)
        {
            System.out.println(i);
        }

        System.out.println(line());

        // Zeichenketten
        String s1 = "hallo";
        String s2 = "welt";
        String s3 = "Hallo";
        String s4 = "Hallo Welt!";

        TreeSet<String> treeSet1 = new TreeSet<>();

        treeSet1.add(s1);
        treeSet1.add(s2);
        treeSet1.add(s3);
        treeSet1.add(s4);

        for (String s: treeSet1)
        {
            System.out.println(s);
        }

        System.out.println(line());

        // Autos

        Auto a1 = new Auto(0,100, "rot");
        Auto a2 = new Auto(10,160, "blau");
        Auto a3 = new Auto(20,140, "grün");
        Auto a4 = new Auto(30,110, "gelb");

        TreeSet<Auto> treeSet2 = new TreeSet<>();

        treeSet2.add(a1);
        treeSet2.add(a2);
        treeSet2.add(a3);
        treeSet2.add(a4);

        for (Auto x: treeSet2)
        {
            System.out.println(x);
        }

        System.out.println(line());

        // Autos mit Comparatoren

        // beim Programmaufruf wird der Comparator festgelegt
        Comparator<Auto> comparator = new AutoComparatorPS();

        // falls beim Aufruf ein Parameter übergeben wurde
        if(args.length == 1)
        {
            if (args[0].equals("km"))
                comparator = new AutoComparatorKM();

            if (args[0].equals("farbe"))
                comparator = new AutoComparatorFarbe();
        }

        TreeSet<Auto> treeSet3 = new TreeSet<>(comparator);

        treeSet3.add(a1);
        treeSet3.add(a2);
        treeSet3.add(a3);
        treeSet3.add(a4);

        for (Auto x: treeSet3)
        {
            System.out.println(x);
        }

        System.out.println("\n--- descending set ---");

        // absteigend sortieren lassen
        for (Auto auto: treeSet3.descendingSet())
        {
            System.out.println(auto);
        }

        System.out.println("\n--- descending iterator ---");

        Iterator<Auto> itr = treeSet3.descendingIterator();

        while (itr.hasNext())
        {
            System.out.println(itr.next());
        }

        System.out.println(line());
    }
}