package de.karbaum.uebung;

/**
 * Created by dev on 01.07.16.
 */
public class Auto implements  Comparable<Auto>
{
    private int kilometerstand;
    private int motorleistung;
    private String farbe;

    public Auto(int kilometerstand, int motorleistung, String farbe)
    {
        this.kilometerstand = kilometerstand;
        this.motorleistung = motorleistung;
        this.farbe = farbe;
    }

    @Override
    public String toString()
    {
        return String.format("%s, %d, %d, %s", this.getClass().getSimpleName(),kilometerstand, motorleistung, farbe);
    }

    @Override
    public int compareTo(Auto o)
    {
        // Vergleicht das aktuelle Auto (this) mit einem anderen Auto o (other)
        // return this.kilometerstand - o.kilometerstand;
        return this.farbe.compareTo(o.farbe);
    }

    // Getter-Methoden für die Eigenschaften

    public int getKilometerstand()
    {
        return kilometerstand;
    }

    public int getMotorleistung()
    {
        return motorleistung;
    }

    public String getFarbe()
    {
        return farbe;
    }
}
