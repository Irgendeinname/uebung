package de.karbaum.uebung.controller;

import de.karbaum.uebung.model.Salary;

import java.io.*;

import java.sql.*;

import java.util.*;

/**
 * Created by dev on 30.06.16.
 */
public class SalaryController
{
    private Connection conn;
    private Set<Salary> salarySet;
    ArrayList al = new ArrayList();
    private Integer i = 1;

    // Dient zum Aufbau der Verbindung mit Datenbank
    public SalaryController()
    {
        // In diesem Set werden alle Gehälter verwaltet
        salarySet = new HashSet<>();

        // Eigenschaften die zum Anmelden an die Datenbank dienen
        Properties properties = new Properties();

        properties.put("user", "root");
        properties.put("password", "root");

        // Aufbau der Verbindung mittels Driver Manger und jdbc
        try
        {
            this.conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/employees", properties);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    public void retrieveAVGSalaries()
    {
        if (conn == null)
            throw new RuntimeException("Keine Verbindung zur Datenbank");

        PreparedStatement preparedStatement = null;

        try
        {
            // String query = "SELECT AVG(salary) AS Durchschnittsgehalt FROM salaries WHERE to_date LIKE '9999-01-01'";
            String query = "SELECT salaries.salary, employees.first_name, employees.last_name  " +
                           "FROM salaries " +
                           "INNER JOIN employees " +
                           "ON salaries.emp_no = employees.emp_no " +
                           "WHERE salaries.to_date " +
                           "LIKE '9999-01-01' LIMIT ?, ?";
            preparedStatement = conn.prepareStatement(query);

            boolean completed = false;

            for (int offset = 0; !completed; offset += 100)
            {
                try
                {
                    preparedStatement.setInt(1, offset);
                    preparedStatement.setInt(2, 100);

                    ResultSet rs = preparedStatement.executeQuery();

                    if (rs.next() == false)
                    {
                        completed = true;
                        continue;
                    }

                    rs.beforeFirst();

                    System.out.println("\t<<< Chunk >>>" + i);
                    i ++;

                    while (rs.next())
                    {
                        //al.add(rs.getString("Durchschnittsgehalt"));
                        al.add(rs.getString("salary"));
                        al.add(rs.getString("first_name"));
                        al.add(rs.getString("last_name"));
                    }
                }
                catch (SQLException e)
                {
                    e.printStackTrace();
                }

                writeToTXT(al, new File("/home/dev/Projects/mavenProject/avg_salaries.txt"));
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    public void writeToTXT(List liste, File datei)
    {
        int i = 0;
        PrintWriter printWriter = null;
        try
        {
            printWriter = new PrintWriter(new FileWriter(datei));
            Iterator iter = liste.iterator();

            while (iter.hasNext())
            {
                Object o = iter.next();
                printWriter.println(o);
                i ++;

                if (i == 3)
                {
                    printWriter.println("---------------------");
                    i = 0;
                }
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        finally
        {
            if (printWriter != null)
                printWriter.close();
        }

    }

    /*** // Dient zur Abfrage der Gehälter aus der Datenbank
    public void retrieveSalaries()
    {
        // Besteht die Verbindung?
        if (conn == null)
            throw new RuntimeException("Keine Verbindung zur Datenbank");

        // Abfrage durch PreparedStatement
        PreparedStatement prepState = null;

        // Versuch der Abfrage der Gehälter, mittels PreparedStatement
        try
        {
            String query = "SELECT * FROM salaries LIMIT ?, ?";
            prepState = conn.prepareStatement(query);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        // Boolvariable um Ende der Abfrage festzustellen
        boolean completed = false;

        // Schleife wenn noch nicht fertig, es wird immer um 100 Datensätze weiter gesprungen
        for (int offset = 0; !completed; offset += 100)
        {
            try
            {
                prepState.setInt(1, offset);
                prepState.setInt(2, 100);

                ResultSet rs = prepState.executeQuery();

                // kann kein weiterer Datensatz abgerufen werden wird die Boolvariable completed gesetzt
                if (rs.next() == false)
                {
                    completed = true;
                    continue;
                }

                // kann ein weiterer Datensatz abgerufen werden springt der Zähler auf die vorherige Position zurück
                // somit wird sichergestell das alle Datensätze erfasst werden
                rs.beforeFirst();

                // Optischer Hinweis, somit sieht der Nutzer dass das Programm läuft
                System.out.println("\t<<< Chunk >>>");

                // Schleife zur eigentlichen Abfrage
                while (rs.next())
                {
                    Integer emp_no = rs.getInt(1);
                    Integer salary = rs.getInt(2);
                    java.util.Date from_date = DateController.fromString(rs.getString(3)); // Datum wird in ein Java
                    java.util.Date to_date = DateController.fromString(rs.getString(4));   // verträgliches Format
                                                                                           // umgewandelt
                    Salary sal = new Salary(emp_no, salary, from_date, to_date);

                    // Daten werden dem Set hinzugefügt
                    salarySet.add(sal);
                }

                writeToCSV();
                rs.close();
            }
            catch (SQLException e)
            {
                e.printStackTrace();
            }
        }
    }

    public void writeToCSV()
    {
        try(BufferedWriter writer = new BufferedWriter(new FileWriter("salaries.csv", true)))
        {
            for (Salary s: salarySet)
            {
                writer.write(s.asCSV());
                writer.newLine();
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        salarySet.clear();
    }***/
}
