package de.karbaum.uebung.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Date;

/**
 * Created by dev on 29.06.16.
 */
public class DateController
{
    public static Date fromString(String datum)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        Date retDatum = null;

        try
        {
            retDatum = sdf.parse(datum);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }

        return retDatum;
    }

    public static String toString(Date datum)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");

        return sdf.format(datum);
    }
}
