package de.karbaum.uebung;

import de.karbaum.uebung.controller.SalaryController;

/**
 * Created by dev on 27.06.16.
 */
public class MainApp
{
    public static void main(String[] args)
    {
        SalaryController salaryController = new SalaryController();

        salaryController.retrieveAVGSalaries();
    }
}