package de.karbaum.uebung.model;

import de.karbaum.uebung.controller.DateController;

import java.util.Date;

/**
 * Created by dev on 30.06.16.
 */
public class Salary
{
    private Integer emp_no;
    private Integer salary;
    private Date from_date;
    private Date to_date;

    public Salary()
    {
    }

    public Salary(Integer emp_no, Integer salary, Date from_date, Date to_date)
    {
        this.emp_no = emp_no;
        this.salary = salary;
        this.from_date = from_date;
        this.to_date = to_date;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)
            return false;

        if (obj == obj)
            return true;

        Salary tmp = (Salary) obj;

        if (!this.emp_no.equals(tmp.emp_no))
            return false;

        if (!this.salary.equals(tmp.salary))
            return false;

        if (!this.from_date.equals(tmp.from_date))
            return false;

        if (!this.to_date.equals(tmp.to_date))
            return false;

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = 5381;

        result = result * 37 + emp_no.hashCode();
        result = result * 37 + salary.hashCode();
        result = result * 37 + from_date.hashCode();
        result = result * 37 + to_date.hashCode();

        return result;
    }

    /***public String asCSV()
    {
        return String.format("%s, %s, %s, %s",
                this.emp_no,
                this.salary,
                DateController.toString(this.from_date),
                DateController.toString(this.to_date));
    }***/
}
