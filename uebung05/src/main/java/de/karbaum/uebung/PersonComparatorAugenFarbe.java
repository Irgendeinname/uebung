package de.karbaum.uebung;

import java.util.Comparator;

/**
 * Created by dev on 01.07.16.
 */
public class PersonComparatorAugenFarbe implements Comparator<Person>
{
    @Override
    public int compare(Person o1, Person o2)
    {
        return o1.getAugenfarbe().compareTo(o2.getAugenfarbe());
    }
}