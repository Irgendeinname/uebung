package de.karbaum.uebung;

import java.sql.Array;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.TreeSet;

/**
 * Created by dev on 27.06.16.
 */
public class MainApp
{
    // Comparator
    public static void main(String[] args)
    {
        Comparator<Person> comparator = new PersonComparatorGroeße();

        if(args.length == 1)
        {
            if (args[0].equals("gewicht"))
                comparator = new PersonComparatorGewicht();

            if (args[0].equals("augenfarbe"))
                comparator = new PersonComparatorAugenFarbe();

            if (args[0].equals("haarfarbe"))
                comparator = new PersonComparatorHaarFarbe();
        }



        // Augen- und Haarfarbe
        String [] augen = new String[5];
        augen[0] = "blau";
        augen[1] = "braun";
        augen[2] = "grün";
        augen[3] = "hellblau";
        augen[4] = "blaugrau";

        String [] haare = new String[5];
        haare[0] = "schwarz";
        haare[1] = "braun";
        haare[2] = "blond";
        haare[3] = "grau";
        haare[4] = "hellbraun";



        // ArrayList
        ArrayList<Person> arrayList = new ArrayList<>();

        for (int i = 0; i < 20; i++)
        {
            arrayList.add(new Person((int) ((Math.random() +1) *100),
                    (int) ((Math.random() + 0.2) * 100),
                    augen[(int) ((Math.random() * 5))],
                    haare[(int) ((Math.random() * 5))]));
        }

        arrayList.sort(comparator);

        System.out.println("--- arraylist ---\n");

        for (Person x: arrayList)
        {
            System.out.println(x);
        }



        // TreeSet
        TreeSet<Person> treeSet = new TreeSet<>(comparator);

        for (int i = 0; i < 20; i++)
        {
           treeSet.add(new Person((int) ((Math.random() +1) *100),
                   (int) ((Math.random() + 0.2) * 100),
                   augen[(int) ((Math.random() * 5))],
                   haare[(int) ((Math.random() * 5))]));
        }

        System.out.println("\n--- asc set ---\n");

        for (Person x: treeSet)
        {
            System.out.println(x);
        }

        System.out.println("\n--- desc set ---\n");

        for (Person x: treeSet.descendingSet())
        {
            System.out.println(x);
        }
    }
}