package de.karbaum.uebung;

import java.util.Comparator;

/**
 * Created by dev on 01.07.16.
 */
public class PersonComparatorGroeße implements Comparator<Person>
{
    @Override
    public int compare(Person o1, Person o2)
    {
        return o1.getGroeße() - o2.getGroeße();
    }
}