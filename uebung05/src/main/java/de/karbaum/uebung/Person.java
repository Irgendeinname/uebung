package de.karbaum.uebung;

/**
 * Created by dev on 01.07.16.
 */
public class Person implements  Comparable<Person>
{
    private int groeße;
    private int gewicht;
    private String augenfarbe;
    private String haarfarbe;

    public Person(int groeße, int gewicht, String augenfarbe, String haarfarbe)
    {
        this.groeße = groeße;
        this.gewicht = gewicht;
        this.augenfarbe = augenfarbe;
        this.haarfarbe = haarfarbe;
    }

    @Override
    public String toString()
    {
        return String.format("%s, %d, %d, %s, %s", this.getClass().getSimpleName(),groeße, gewicht, augenfarbe, haarfarbe);
    }

    @Override
    public int compareTo(Person o)
    {
        return this.augenfarbe.compareTo(o.augenfarbe);
    }

    public int getGroeße()
    {
        return groeße;
    }

    public int getGewicht()
    {
        return gewicht;
    }

    public String getAugenfarbe()
    {
        return augenfarbe;
    }

    public String getHaarfarbe()
    {
        return haarfarbe;
    }
}
