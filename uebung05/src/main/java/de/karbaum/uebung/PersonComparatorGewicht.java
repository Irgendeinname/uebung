package de.karbaum.uebung;

import java.util.Comparator;

/**
 * Created by dev on 01.07.16.
 */
public class PersonComparatorGewicht implements Comparator<Person>
{
    @Override
    public int compare(Person o1, Person o2)
    {
        return o1.getGewicht() - o2.getGewicht();
    }
}
