package de.karbaum.uebung;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.sql.*;
import java.util.ArrayList;

/**
 * Created by dev on 27.06.16.
 */
public class MainApp
{
    public static void main(String[] args) throws Exception
    {
        int i = 1;

        Connection db = DriverManager.getConnection
                ("jdbc:mysql://localhost:3306/plz",
                        "root",
                        "root");

        String insertSQL = "INSERT INTO plz"
                                + "(id, osm, name, plz, land) VALUES"
                                + "(?, ?, ?, ?, ?)";

        PreparedStatement prepState = db.prepareStatement(insertSQL);

        // Datei im JAR-File einlesen
        String filename = "postleitzahl.csv";

        URL data = MainApp.class.getResource("/" + filename);

        // öffnen der Datei
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(data.openStream())))
        {
            // Anwendungslogik zum Einfügen der Dateien
            String inputLine;

            while ((inputLine = bufferedReader.readLine()) != null)
            {
                String [] parts = inputLine.split(",");

                prepState.setInt(1, i);
                prepState.setString(2, parts[0]);
                prepState.setString(3, parts[1]);
                prepState.setString(4, parts[2]);
                prepState.setString(5, parts[3]);
                i++;

                prepState.executeUpdate();
            }
        }
        catch (IOException e)
        {
            System.err.println(e.getMessage());
        }

        // Abfragen Anzahl der Postleitzahlen
        String selectPLZSQL = "SELECT COUNT(DISTINCT plz) AS anzahlPLZ FROM plz";

        PreparedStatement prepSelPLZState = db.prepareStatement(selectPLZSQL);
        ResultSet resPLZSet = prepSelPLZState.executeQuery();

        while (resPLZSet.next())
        {
            String anzahl = resPLZSet.getString("anzahlPLZ");
            System.out.println("Es gibt insgesamt: " + anzahl + " Postleitzahlen in der Tabelle\n");
        }

        // Abfrage Anzahl Staedte
        String selectCiSQL = "SELECT COUNT(DISTINCT name) AS anzahlStaedte FROM plz";

        PreparedStatement prepSelCiState = db.prepareStatement(selectCiSQL);
        ResultSet resCiSet = prepSelCiState.executeQuery();

        while (resCiSet.next())
        {
            String anzahl = resCiSet.getString("anzahlStaedte");
            System.out.println("Es gibt insgesamt: " + anzahl + " Staedte in der Tabelle\n");
        }

        // Abfrage Anzahl Staedte pro Bundesland
        String selectBulaSQL = "SELECT land, COUNT(DISTINCT name) AS anzahlStaedte FROM plz GROUP BY land WITH ROLLUP";

        PreparedStatement prepSelBulaState = db.prepareStatement(selectBulaSQL);
        ResultSet resBulaSet = prepSelBulaState.executeQuery();

        ArrayList al = new ArrayList();

        while (resBulaSet.next())
        {
            al.add(resBulaSet.getString("land"));
            al.add(resBulaSet.getString("anzahlStaedte"));
        }

        System.out.println(al);

        String selectPLZmitVarSQL = "SELECT plz, name, land FROM plz WHERE plz LIKE ?";

        PreparedStatement prepSelPLZmitVarState = db.prepareStatement(selectPLZmitVarSQL);

        String bereich = "0";
        prepSelPLZmitVarState.setString(1, bereich + "%");

        ResultSet resPLZmitVarSet = prepSelPLZmitVarState.executeQuery();

        ArrayList almitVar = new ArrayList();

        while (resPLZmitVarSet.next())
        {
            almitVar.add(resPLZmitVarSet.getString("plz"));
            almitVar.add(resPLZmitVarSet.getString("name"));
            almitVar.add(resPLZmitVarSet.getString("land"));
        }

        System.out.println(almitVar);
    }
}